"use strict";

// puntuaciones
const puntuaciones = [
  {
    equipo: "Toros Negros",
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipo: "Amanecer Dorado",
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipo: "Águilas Plateadas",
    puntos: [5, 8, 3, 2, 5, 3],
  },
  {
    equipo: "Leones Carmesí",
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipo: "Rosas Azules",
    puntos: [2, 1, 3, 1, 4, 3, 4],
  },
  {
    equipo: "Mantis Verdes",
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipo: "Ciervos Celestes",
    puntos: [3, 5, 1, 1],
  },
  {
    equipo: "Pavos Reales Coral",
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipo: "Orcas Moradas",
    puntos: [2, 3, 3, 4],
  },
];


function getWinnerAndLosser(array) {
  let puntosTotales = array
  .map((puntuacion)=> puntuacion.puntos.reduce((acc,value)=>acc += value));

  let maxPoint = Math.max (...puntosTotales);
  let minPoint = Math.min (...puntosTotales);

  let indexMax = puntosTotales.indexOf(maxPoint);
  let indexMin = puntosTotales.indexOf(minPoint);

  return (
    `El equipo con mayor puntacion es : ${puntuaciones[indexMax].equipo} con ${maxPoint} puntos \nEl equipo con menor puntacion es : ${puntuaciones[indexMin].equipo} con ${minPoint} puntos`
  );
    
};

console.log(getWinnerAndLosser(puntuaciones));
