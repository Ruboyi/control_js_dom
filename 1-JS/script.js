/* # Ejercicio JavaScript

Edita el archivo script.js para crear una función que haga lo siguiente:

- Generar una contraseña (número entero aleatorio del 0 al 100)
- Pedir al usuario que introduzca un número dentro de ese rango.
- Si el número introducido es igual a la contraseña, aparecerá en pantalla un mensaje indicando que ha ganado; si no, el mensaje indicará si la contraseña en un número mayor o menor al introducido y dará una nueva oportunidad.
- El usuario tendrá 5 oportunidades de acertar, si no lo consigue, aparecerá un mensaje indicando que ha perdido.
 */

"use strict";

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

let password = getRandomInt(0, 100);
console.log(password);

let userNumb = Number(prompt("Introduzca un numero entero del 0 al 100"));

let i = 0;

while (userNumb !== password && i < 4) {
  
  i++;

  if (password > userNumb) {
    userNumb = Number(
    prompt(`Su numero introducido es menor,vuelva a intentarlo (Le quedan ${5 - i} intentos):`));
  }else {
    userNumb = Number(
    prompt(`Su numero introducido es mayor,vuelva a intentarlo (Le quedan ${5 - i} intentos):`));
  }
  
};

if (userNumb === password){
  alert("Enhorabuena has ganado !! :");
}else{
  alert("Lo siento has perdido");
};