'use strict';

const formatNum = (num) => {
    return num < 10 ? '0' + num : num;
};


const body = document.querySelector('body');
const clock = document.createElement('div');
body.append(clock);
clock.style.cssText = 'font-size: 200px; background-color: black;color: white;min-height: 100vh;display: grid;place-content: center;background-size: cover;background-position: center;'

const interval = setInterval(() => {

    const currentDate = new Date();
    const hour = currentDate.getHours();
    const minute = currentDate.getMinutes();
    const second = currentDate.getSeconds();
    clock.textContent =`${formatNum(hour)}:${formatNum(minute)}:${formatNum(second)}`;

}, 1000);



